from django.urls import path
from projects.views import get_project, display_project, create_project

urlpatterns = [
    path("", get_project, name="list_projects"),
    path("<int:id>/", display_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
